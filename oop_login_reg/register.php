<?php
   session_start();
   require_once"functions.php";
   $user = new LoginRegistration();
   if($user->getSession()){
             header('Location: index.php');
             exit();
         }
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registration Page</title>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
	 <div class="wrapper"> 

        <div class="header">
        	<h3>PHP OOP Login-Register System</h3>
        </div>

        <div class="mainmenu">
        	<ul>
               <?php if($user->getSession()){?>

                <li><a href="index.php">Home</a></li>
                <li><a href="profile.php">Show Profile</a></li>
                <li><a href="changePassword.php">Change Password</a></li>
                <li><a href="logout.php">Logout</a></li>

                <?php } else { ?>

                <li><a href="login.php">Login</a></li>
                <li><a href="register.php">Register</a></li>
                
                <?php } ?>
            </ul>
        </div>

        <div class="content">
        	<h2>Register</h2>
        
        <p class="msg">
        	<?php
                if($_SERVER['REQUEST_METHOD']=="POST"){
                    $username = $_POST['username'];
                    $password = $_POST['password'];
                    $name     = $_POST['name'];
                    $email    = $_POST['email'];
                    $website  = $_POST['website'];

                    if(empty($username) or empty($password) or empty($name ) or empty($email ) or empty($website )){
                          echo "<span style='color:#e53d37'>Error...Filled must not be empty</span>";
                    }
                    else{
                        $password = md5($password);
                        $register = $user->registerUser($username,$password,$name,$email,$website);
                         
                        if($register){
                            echo "<span style='color:green'>Register Done <a href='login.php'>Click Here</a> for login.</span>";
                        }else{
                            echo"<span style='color:#e53d37'>Username or Email already exists.</span>";
                        }
                    }
                }
            ?>
        </p>
        <div class="login_reg">
        	<form action="" method="post">
        		<table>
        			<tr>
        				<td>Username:</td>
                        <td><input type="text" name="username" placeholder="Please Enter Your Username"></td>
        			</tr>

                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="password" placeholder="Please Enter Your Password"></td>
                    </tr>

                    <tr>
                        <td>Name:</td>
                        <td><input type="text" name="name" placeholder="Please Enter Your Name"></td>
                    </tr>

                    <tr>
                        <td>Email:</td>
                        <td><input type="email" name="email" placeholder="Please Enter Your Email"></td>
                    </tr>

                    <tr>
                        <td>Website:</td>
                        <td><input type="text" name="website" placeholder="Please Enter Your Website"></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                        <span style="float:right">
                            <input type="submit" name="register" value="Register">
                            <input type="reset"  value="Reset">
                        </span>
                        </td>
                    </tr>
        		</table>
        	</form>
        </div>

            <div class="back">
                <a href="login.php"><img src="img/back.png" alt="back"></a>
            </div>
        </div>    

    <div class="footer">
        <h3>www.raHMat project.com</h3>
    </div>

	 </div>
</body>
</html>